@extends('layouts.main')

@section('title', 'Subir imagen')

@section('content')

    <h1>Subir imagen</h1>

    <form action="{{ route('site.store') }}" method="post" enctype="multipart/form-data">

        @csrf

        <div class="form-group">
            <label for="image">Imagen</label>
            <input type="file" name="image" id="image" class="form-control">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Subir</button>
        </div>
    </form>
@endsection
