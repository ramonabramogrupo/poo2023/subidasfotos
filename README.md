# OBJETIVOS
Vamos a realizar una aplicacion que sea capaz de subir fotos a una carpeta y un visualizar de las fotos de la carpeta. 

Ademas vamos a realizar otro formulario para subir las fotos y ademas de almacenarlas en una carpeta guardarlas en la base de datos

# INSTALACION

Creamos un nuevo proyecto de laravel

~~~php
laravel new subidas
~~~ 

# CONFIGURACION 

En el fichero .env 

~~~php
APP_LOCALE=es
APP_FALLBACK_LOCALE=es
APP_FAKER_LOCALE=es_ES
~~~

# Instalar dependencias de node

Entramos en la carpeta de la aplicacion. 

Podemos ejecutar cualquiera de estos dos comandos

~~~php
npm i
~~~

# Instalar SASS y BOOTSTRAP

Para esta aplicacion voy a utilizar SASS y Bootstrap
~~~php
npm i sass --save-dev
npm i bootstrap --save-dev
~~~

# Modificar CSS y JS para utilizar bootstrap y sass

Como ya hemos visto en clase necesitamos importar los css de bootstrap y el js de bootstrap

```php
import './bootstrap';

/**
 * cargar js bootstrap
 */

import * as bootstrap from 'bootstrap';
```

```php
/*
cargar estilos bootstrap
*/

@import "bootstrap/scss/bootstrap";
```

# Configurar el archivo vite.config.js

En este archivo debo incoporar el archivo app.scss

~~~php
import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/css/app.scss', 'resources/js/app.js'],
            refresh: true,
        }),
    ],
});
~~~

# CONTROLADOR SITE

Creo un controlador site para las acciones principales de la aplicacion

~~~php
php artisan make:controller SiteController
~~~

En este controlador colocare las siguientes acciones:
- index :cargar pagina principal. en esta accion voy a colocar para que cargue la vista index que creare mas tarde
- create : es capaz de presentar un formulario para subir una foto sin base de datos
- store : coloca la imagen en la carpeta de fotos
- show : muestra todas las imagenes de la carpeta

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        return view('site.index');
    }

    public function create(){

    }

    public function store(){

    }

    public function show(){

    }

    public function show1(){
        
    }
    
}

```


# CREAMOS EL LAYOUT

Vamos a crear un layout principal denominado main para todas las vistas

Creo una carpeta denominada layouts en resources/views


```plantuml
folder resources/views/layouts {
artifact main.blade.php
}
```

```php
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

</head>

<body>
    
    @yield('content')

</body>

</html>
```

# Fichero de rutas

En el fichero de rutas voy a colocar para que cargue la accion index del controlador site

```php
<?php

use App\Http\Controllers\SiteController;
use Illuminate\Support\Facades\Route;

Route::controller(SiteController::class)->group(function () {
    Route::get('/', 'index')->name('site.index');
});

```

# Vista index del controlador site

Vamos a crear en la carpeta views una carpeta denominada site para las vistas de este controlador

```plantuml
folder views/site {
artifact index.blade.php
}
```

```php
@extends('layouts.main')

@section('title', 'Inicio')

@section('content')
    <h1>Pagina de inicio </h1>

@endsection
```

# Primera prueba

Ya podemos probar en el navegador la siguiente url

> http://subidas.test


# Modificamos el layout

Vamos a modificar el layout para cargar el menu los js y los css. 



# Creamos accion create controlador site

En el controlador SiteController vamos a crear la accion create que nos muestre un formulario para subir una imagen




#  GESTION DE LAS FOTOS PARA ALMACENAR EN LA BBDD

Tenemos que crear los siguientes elementos para subir imagenes y almacenarlas en la bbdd :

- modelo
- migracion
- factories
- seeders
- controlador de recursos

~~~php
php artisan make:model Foto -mfscr
~~~

# GESTION DEL ARCHIVO DE RUTAS

Tenemos que configurar el archivo de rutas






