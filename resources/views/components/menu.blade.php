@props([
    'items' => [
        [
            'href' => '#',
            'label' => 'Inicio',
        ],
    ],
    'titulo' => 'Aplicacion',
])

<nav class="navbar navbar-expand-lg bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">{{ $titulo }}</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                @foreach ($items as $item)
                    <a class="nav-link" href="{{ $item['href'] }}">{{ $item['label'] }}</a>
                @endforeach
            </div>
        </div>
    </div>
</nav>
